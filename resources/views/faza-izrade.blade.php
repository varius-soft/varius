<!doctype html>
<html lang="en">
    
<!-- Mirrored from envytheme.com/tf-demo/turacos/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Apr 2019 23:32:11 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- IcoFont Min CSS -->
        <link rel="stylesheet" href="assets/css/icofont.min.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="assets/css/animate.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <!-- Owl Theme Default CSS -->
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <title>Varius - Izrada Web sajtova, SEO Optimizacija,Izrada Internet Prodavnica,Google Reklamiranje, Drustvene Mreze </title>
    </head>
    
    <body>
        <!-- Preloader Area -->
        <div class="preloader-area">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
        <!-- End Preloader Area -->


        

        <!-- End Right Side Modal -->
        <!-- Page Title -->
        <div style="height: 1200PX;" class="page-title animatedBackground">
            <div class="container">
                <img style="display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" src="{{asset('assets/img/logo.png')}}">
                <h3>SAJT JE U FAZI REKONSTURKCIJE.</h3>
                <ul>
                    
                    <li>MOŽETE NAS KONTAKTIRATI PUTEM MAILA ILI TELEFONA.</li>
                    <br>
                    <li><a href="mailto:info@varius-soft.com">info@varius-soft.com</a></li>
                    <br>
                    <li> <a href="tel:+38163699165">(+381) 063 699 165</a></li>
                </ul>
            </div>
            
            
        </div>
        <!-- End Page Title -->
        
        
        

        
        <div class="go-top"><i class="icofont-stylish-up"></i></div>
        
        <!-- jQuery Min JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Prpper JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap Min JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Owl Carousel Min Js -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Owl Carousel Thumbs JS -->
        <script src="assets/js/owl.carousel2.thumbs.min.js"></script>
        <!-- Jquery Magnific Popup Min Js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Jquery Mixitup Min Js -->
        <script src="assets/js/jquery.mixitup.min.js"></script>
        <!-- Waypoints Min Js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- Jquery CounterUp Min JS -->
        <script src="assets/js/jquery.counterup.min.js"></script>
        <!-- ajaxChimp Min JS -->
        <script src="assets/js/jquery.ajaxchimp.min.js"></script>
        <!-- Form Validator Min JS -->
        <script src="assets/js/form-validator.min.js"></script>
        <!-- Contact Form Min JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- Fox Map JS FILE -->
        <script src="assets/js/turacos-map.js"></script>
        <!-- Main JS -->
        <script src="assets/js/main.js"></script>
    </body>

<!-- Mirrored from envytheme.com/tf-demo/turacos/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Apr 2019 23:32:42 GMT -->
</html>













        
        