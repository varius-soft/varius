<!doctype html>
<html lang="en">
    
<!-- Mirrored from envytheme.com/tf-demo/turacos/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Apr 2019 23:32:11 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- IcoFont Min CSS -->
        <link rel="stylesheet" href="assets/css/icofont.min.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="assets/css/animate.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <!-- Owl Theme Default CSS -->
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <title>Varius - Izrada Web sajtova, SEO Optimizacija,Izrada Internet Prodavnica,Google Reklamiranje, Drustvene Mreze </title>
    </head>
    
    <body>
        <!-- Preloader Area -->
        <div class="preloader-area">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
        <!-- End Preloader Area -->
		
		<!-- Header Area -->
		<header class="header-area">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-5 col-md-5">
		                <ul class="social-links">
                            <li>Pratite nas:</li>
                            <li><a href="#"><i class="icofont-facebook"></i></a></li>
                            <li><a href="#"><i class="icofont-twitter"></i></a></li>
                            <li><a href="#"><i class="icofont-instagram"></i></a></li>
                            <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                            <li><a href="#"><i class="icofont-pinterest"></i></a></li>
                        </ul>
		            </div>
		            
		            <div class="col-lg-7 col-md-7">
		                <ul class="header-info">
		                    <li>Kontaktirajte nas</li>
		                    <li><a href="mailto:info@varius-soft.com">info@varius-soft.com</a></li>
		                    <li> <a href="tel:+38163699165">(+381) 063 699 165</a></li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</header>
        <!-- End Header Area -->
		
        <!-- Navbar Area -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-md-12">

                        <a  href="/">
                            <img height="45" src="{{asset('assets/img/logo.png')}}">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">                                
                                <li class="nav-item dropdown active">
                                    <a class="nav-link " href="/" role="button" >
                                        Početna
                                    </a>
                                    
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="o-nama" role="button" >
                                        O Nama
                                    </a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        Usluge
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="/">Izrada Web Sajtova</a>
                                        <a class="dropdown-item" href="/">SEO</a>
                                        <a class="dropdown-item" href="/">Google Reklamiranje</a>
                                        <a class="dropdown-item" href="/">Vođenje društvenih mreža</a>
                                        <a class="dropdown-item" href="/">Grafički dizajn</a>
                                        <a class="dropdown-item" href="/">Copywriting</a>
                                        <a class="dropdown-item" href="/">marketing</a>
                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="#" role="button" >
                                        VS Shop
                                    </a>
                                    
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="#" role="button" >
                                        Klijenti
                                    </a>
                                    
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="#" role="button" >
                                        Portfolio
                                    </a>
                                    
                                </li>
                                
                                <li class="nav-item"><a class="nav-link" href="contact.html">Kontakt</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-12">
                        <div class="navbar-right-side">
                            <div class="modal-taggle-button">
                                <a href="#" data-toggle="modal" data-target="#myModal2"><span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- End Navbar Area -->
		
                <!-- Right Side Modal -->
        <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="logo">
                            <a href="/">
                            <img src="{{asset('assets/img/logo.png')}}">
                            </a>
                        </div>
                        <p>Turacos have much planned for the future, working with great clients and continued software development. If you'd like to join our team, then we'd also love to hear from you.</p>
                        <ul class="modal-contact-info">
                            <li><i class="icofont-google-map"></i> <b>27 Division St</b>New York, NY 10002, USA</li>
                            <li><i class="icofont-ui-call"></i> <b>+0 (321) 984 754</b>Give us a call</li>
                            <li><i class="icofont-envelope"></i> <b>turacos@gmail.com</b>24/7 online support</li>
                        </ul>

                        <ul class="social-links">
                            <li><a href="#"><i class="icofont-facebook"></i></a></li>
                            <li><a href="#"><i class="icofont-twitter"></i></a></li>
                            <li><a href="#"><i class="icofont-instagram"></i></a></li>
                            <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                            <li><a href="#"><i class="icofont-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Right Side Modal -->
		@yield('sekcije')
        
        <!-- Footer Area -->
        <footer class="footer-area bg-fbf9f8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <div class="logo">
                                <a href="/">
                                    <img src="{{asset('assets/img/logo.png')}}">
                                </a>
                            </div>
                            <p>Turacos have much planned for the future, working with great clients and continued software development. If you'd like to join our team, then we'd also love to hear from you.</p>
                            <ul class="social-links">
                                <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                <li><a href="#"><i class="icofont-instagram"></i></a></li>
                                <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                                <li><a href="#"><i class="icofont-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <h3>Services</h3>
                            <ul class="services-list">
                                <li><a href="#">Strategy & Research</a></li>
                                <li><a href="#">Reports & Analytics</a></li>
                                <li><a href="#">Managment & Marketing</a></li>
                                <li><a href="#">Infographics Content</a></li>
                                <li><a href="#">Media Promotion</a></li>
                                <li><a href="#">Content Marketing</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <h3>Usefull links</h3>
                            <ul class="usefull-links">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Team</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <h3>Contacts</h3>
                            <ul class="contact-list">
                                <li><span>Adress:</span> 27 Division St, New York, NY 10002, USA</li>
                                <li><span>Website:</span> <a href="#">turacos.com</a></li>
                                <li><span>Email:</span> <a href="#">turacos@gmail.com</a></li>
                                <li><span>Phone:</span> <a href="#">+0 (321) 984 754</a></li>
                                <li><span>Fax:</span> <a href="#">+0 (321) 984 754</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <ul>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                        
                        <div class="col-lg-6 col-md-6 text-right">
                            <p>Copyright @2019 Turacos. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Area -->
        
        <div class="go-top"><i class="icofont-stylish-up"></i></div>
        
        <!-- jQuery Min JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Prpper JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap Min JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Owl Carousel Min Js -->
        <script src="assets/js/owl.carousel.min.js"></script>
		<!-- Owl Carousel Thumbs JS -->
        <script src="assets/js/owl.carousel2.thumbs.min.js"></script>
        <!-- Jquery Magnific Popup Min Js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Jquery Mixitup Min Js -->
        <script src="assets/js/jquery.mixitup.min.js"></script>
        <!-- Waypoints Min Js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- Jquery CounterUp Min JS -->
        <script src="assets/js/jquery.counterup.min.js"></script>
		<!-- ajaxChimp Min JS -->
        <script src="assets/js/jquery.ajaxchimp.min.js"></script>
        <!-- Form Validator Min JS -->
        <script src="assets/js/form-validator.min.js"></script>
        <!-- Contact Form Min JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- Fox Map JS FILE -->
        <script src="assets/js/turacos-map.js"></script>
        <!-- Main JS -->
        <script src="assets/js/main.js"></script>
    </body>

<!-- Mirrored from envytheme.com/tf-demo/turacos/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 16 Apr 2019 23:32:42 GMT -->
</html>